require('rootpath')();
var session = require('express-session');
var path = require('path');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var config = require('config.json');
var nforce = require('nforce');
var apiService = require('./services/api.service');


app.use(cors({origin: true, credentials: true}));
app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json());

//app.use(expressJwt({secret: config.secret}).unless({path: ['/users/authenticate', '/users/register']}));

app.use(session({
    secret: 'ssssss',
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 50000
    }
}));

app.use('/api', require('./controllers/api.controller'));
app.use('/auth', require('./controllers/auth.controller'));
app.use(express.static(path.join(__dirname, 'build')));

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build/index.html'));
});


var port = process.env.NODE_ENV === 'production' ? 80 : 5000;

apiService.connected().then(function(org) {


    var server = app.listen(port, function () {
        console.log('Server listening on port ' + port);
    });

}).catch(function(err) {
    console.log(err);
});