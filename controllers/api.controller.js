var config = require('config.json');
var express = require('express');
var router = express.Router();
var apiService = require('services/api.service');
router.post('/authenticate', authenticate);
router.get('/events', getEvents);
router.get('/event/:_eventId', getEvent);
router.get('/event/:_eventId/registered/:_email', checkEmail);
router.get('/attendees/:_eventId', getAttendees);
router.get('/attendee/:_attendeeId', getAttendee);
router.get('/sessions/:_eventId', getSessions);
router.get('/session/:_sessionId', getSession);
router.get('/session_attendees/:_sessionId', getSessionAttendees);
router.put('/attendee/:_attendeeId', updateAttendee);
router.post('/event', addEvent);
router.post('/event/:_eventId/attendee', addAttendee);
router.post('/event/:_eventId/session', addSession);
router.post('/session/:_session_id/:_attendee_id', addAttendeeToSession);

/*router.get('/current', getCurrent);
 router.put('/:_id', update);
 router.delete('/:_id', _delete);*/

module.exports = router;

function addEvent(req, res) {
    var sess = req.session;
    apiService.createEvent(req.body, sess.oauth).then(function (result) {
        res.send({success: true});
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function addSession(req, res) {
    apiService.createSession(req.params['_eventId'], req.body).then(function (result) {
        res.send({success: true});
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function updateAttendee(req, res) {
}

function addAttendee(req, res) {
    apiService.createAttendee(req.params['_eventId'], req.body).then(function (result) {
        res.send({success: true, result: result});
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function checkEmail(req, res) {
    apiService.checkEmail(req.params['_email'], req.params['_eventId']).then(function (result) {
        if (result.length) res.send({attendeeId: result[0]._fields['id']});
        else res.send({attendeeId: null});
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function addAttendeeToSession(req, res) {
    apiService.addAttendeeToSession(req.params['_attendee_id'], req.params['_session_id']).then(function (result) {
        res.send({success: true});
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function authenticate(req, res) {
    apiService.authenticate(req.body.username, req.body.password).then(function (user) {
        if (user) res.send(user);
        else res.status(401).send('Username or password is incorrect');
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getEvents(req, res) {
    apiService.getEvents().then(function (events) {
        res.send(events.map(function (event) {
            return event._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getEvent(req, res) {
    apiService.getEvent(req.params['_eventId']).then(function (event) {
        res.send(event.map(function (event) {
            return event._fields;
        }));
    }).catch(function (err) {
        res.satus(400).send(err);
    })
}

function getAttendees(req, res) {
    apiService.getAttendees(req.params['_eventId']).then(function (attendees) {
        res.send(attendees.map(function (attendee) {
            return attendee._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getAttendee(req, res) {
    apiService.getAttendee(req.params['_attendeeId']).then(function (attendee) {
        res.send(attendee.map(function (attendee) {
            return attendee._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getSession(req, res) {
    apiService.getSession(req.params['_sessionId']).then(function (result) {
        res.send(result.map(function (session) {
            return session._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getSessions(req, res) {
    apiService.getSessions(req.params['_eventId']).then(function (result) {
        res.send(result.map(function (session) {
            return session._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}

function getSessionAttendees(req, res) {
    apiService.getAttendeesBySession(req.params['_sessionId']).then(function (attendees) {
        res.send(attendees.map(function (attendee) {
            return attendee._fields;
        }));
    }).catch(function (err) {
        res.status(400).send(err);
    });
}