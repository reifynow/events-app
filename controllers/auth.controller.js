var config = require('config.json');
var express = require('express');
var router = express.Router();
var apiService = require('services/api.service');

router.get('/sfdc', sfdc);
router.get('/sfdc/callback', callback);
module.exports = router;

function sfdc(req, res) {
    res.redirect(apiService.getAuthUri());
}

function callback(req, res) {
    apiService.authenticate({code: req.query.code}).then(function(result) {
        var sess = req.session;
        sess.oauth = result;
        sess.authToken = "Bearer "+result.access_token;
        sess.save();
        res.send(result);
    }).catch(function(err) {
        res.status(401).send(err);
    });
}
