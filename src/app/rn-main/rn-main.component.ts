import {Component, OnInit} from '@angular/core';
import {AuthService} from '../rn-secure/auth.service';
import {UserService} from './account/user.service';
import {RnMainService} from './rn-main.service';


@Component({
  selector: 'rn-main',
  templateUrl: './rn-main.component.html',
  styles: [``]
})
export class RnMainComponent implements OnInit {
  constructor(private authService : AuthService, private userService: UserService, private mainService : RnMainService) {}

  ngOnInit() {
  }

}
