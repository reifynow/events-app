import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {SecureDataService} from '../../../rn-secure/secure-data.service';

@Injectable()
export class LoginService {
  constructor(private http: Http, private secureDataService : SecureDataService) {
  }

  login(username: string, password: string) : Observable<any> {

    let url = this.secureDataService.getAuthUrl();
    let body =  new URLSearchParams();
    let bodyJson = {
//      "grant_type": "password",
      "username": username,
      "password": password
    };
//    body.set("grant_type", 'password');
    body.set('username',username);
    body.set('password',password);
    let headers = new Headers({
      'Accept' : 'application/json',
//      'Content-Type': 'application/x-www-form-urlencoded'
//      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});
    return this.http.post(url, bodyJson/*"grant_type=password&username="+encodeURIComponent(username)+"&password="+password*/, options)
      .map(this.extractData)
      .catch(this.handleError);

  }

  resetPassword(email: string) : Observable<any> {
    let url = this.secureDataService.getUserApiUrl() + '/users/resetpasswordrequest';
    let headers = new Headers({
      'Accept' : 'application/json',
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers, body: ''});

    return this.http.post(url+'?email='+encodeURIComponent(email), '', options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  resetPasswordFinal(userId: number, password: string, code: string) : Observable<any> {
    let url = this.secureDataService.getUserApiUrl() + '/users/'+userId+'/resetpassword?userId='+userId;
    let headers = new Headers({
      'Accept' : 'application/json',
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});
    return this.http.post(url, JSON.stringify({'new_password':password, 'verification_code':code}), options)
      .map(this.extractData)
      .catch(this.handleError);
  }


  private extractData(res: Response) {
    let body = res.json();
    return body || res;
  }

  private handleError (error: any) {
    let err = error;
    let message = {error_description : false};
    let res : any = {type: 'danger', message: 'Server error.', dismissible: false, timeToLive: null};
    if (err._body) {
      message = JSON.parse(err._body);
    }
    if (message.error_description) res = {type: 'danger', message: message.error_description, dismissible: true, timeToLive: null};
    return Observable.throw(res);
  }

}

