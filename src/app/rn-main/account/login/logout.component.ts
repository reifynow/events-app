import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../rn-secure/auth.service';


@Component({
  selector: 'rn-logout',
  template: ``
})
export class LogoutComponent {
  constructor(private authService: AuthService, private router : Router) {
      authService.setLoggedOut();
      router.navigate(['/home']);
  }
}
