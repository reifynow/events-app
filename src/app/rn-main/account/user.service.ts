import { Injectable }     from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient} from '../../rn-secure/http-client';
import {SecureDataService} from '../../rn-secure/secure-data.service';
import { Observable }     from 'rxjs/Observable';
@Injectable()
export class UserService {

  constructor (private http: HttpClient, private secureDataService : SecureDataService) {}

  getUser() : Observable<any> {
    return this.http.get(this.secureDataService.getUserApiUrl()+'/current')
      .map(res => res.json())
      .catch(this.handleError);
  }

  getUserById(userId : number) : Observable<any> {
    return this.http.get(this.secureDataService.getUserApiUrl()+'/users/'+userId)
      .map(this.extractData)
      .catch(this.handleError);
  }

  createUser(userData : any): Observable<any> {
    return this.http.post(this.secureDataService.getUserApiUrl()+'/users', JSON.stringify(userData))
      .map(this.extractData)
      .catch(this.handleError);
  }

  verifyUser(code : string) : Observable<any> {
    return this.http.post(this.secureDataService.getUserApiUrl()+'/verify/'+code, JSON.stringify({}))
      .map(this.extractData)
      .catch(this.handleError);
  }

  registerUser(userData : any) : Observable<any> {
    return this.http.post(this.secureDataService.getUserApiUrl() + '/users/register', JSON.stringify(userData))
      .map(this.extractData)
      .catch(this.handleError);
  }

  registerInviteUser(userData : any, code : string) : Observable<any> {
    return this.http.post(this.secureDataService.getUserApiUrl() + '/accounts/invites/acceptwithregistration', JSON.stringify({"FirstName":userData.FirstName,LastName:userData.LastName,Email:userData.Email,Password:userData.Password,UserId:userData.UserID,VerificationCode:code}))
      .map(this.extractData)
      .catch(this.handleError);
  }

  completeInvite(userId : number, code: string) : Observable<any> {
    return this.http.post(this.secureDataService.getUserApiUrl() + '/accounts/invites/accept', JSON.stringify({UserId: userId, VerificationCode: code}))
      .map(this.extractData)
      .catch(this.handleError);
  }

  getRates() : Observable<any> {
    return this.http.get(this.secureDataService.getUserApiUrl() + '/rates')
      .map(this.extractData)
      .catch(this.handleError);
  }

  findUser(accountId: number, query: string): Observable<any>{
    return this.http.get(this.secureDataService.getUserApiUrl() + '/accounts/' + accountId + '/users/search/?query=' + query)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    return Observable.throw(error);
  }
}
