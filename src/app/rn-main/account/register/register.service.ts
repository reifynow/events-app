import {Injectable} from '@angular/core';
import {HttpClient} from '../../../rn-secure/http-client';
import {RequestOptions} from '@angular/http';
import {SecureDataService} from '../../../rn-secure/secure-data.service';
import {Observable, ReplaySubject, Subscription} from 'rxjs/Rx';

@Injectable()
export class RegisterService {

  private apiUrl : string;

  constructor(private http : HttpClient, secureDataService : SecureDataService) {
    this.apiUrl = secureDataService.getUserApiUrl();
  }

  create(user : any) : Observable<any> {
    return this.http.post(this.apiUrl+'/register', user, <RequestOptions>{headers : {}});
  }


  /*

  startRegistration(registerObject : any) : Observable<any> {
    return this.http.post(this.apiUrl+'/startregistration', JSON.stringify(registerObject))
      .map(res => res.json());
  }

  registerStepOne(userId: string, confirmationCode: string) : Observable<any> {
    return this.http.post(this.apiUrl+'/register/step1', JSON.stringify({user_id: userId, confirmation_code: confirmationCode}))
      .map(res => res.json());
  }

  registerStepTwo(registerObject: any) : Observable<any> {
    return this.http.post(this.apiUrl+'/register/step2', JSON.stringify(registerObject))
      .map(res => res.json());
  }

  registerSecurity(registerObject : any) : Observable<any> {
    return this.http.post(this.apiUrl+'/register/security', JSON.stringify(registerObject))
      .map(res => res.json());
  }

  resendCode(userId: string) : Observable<any> {
    return this.http.post(this.apiUrl+'/register/confirmation/resend/'+userId, JSON.stringify({}))
      .map(res => res.json());
  } */
}
