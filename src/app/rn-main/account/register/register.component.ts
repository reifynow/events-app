import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {RnAlertData} from '../../../rn-core/components/messaging/alert/rn-alert.component';
import { RegisterService } from './register.service';

@Component({
    selector: 'register-component',
    templateUrl: './register.component.html'
})

export class RegisterComponent {
    model: any = {};
    loading = false;
    alerts : Array<RnAlertData> = [];
    success : boolean = false;
    constructor(
        private router: Router,
        private registerService : RegisterService
        ) { }

    register() {
        this.registerService.create(this.model)
            .subscribe(
                data => {
                    this.alerts.push(new RnAlertData('success', `Registration successful. <a href="/home">Login here</a>.`, null));
//                    this.router.navigate(['/login']);
                    this.success = true;
                },
                error => {
                    this.alerts.push(new RnAlertData('danger', error._body));
                    this.success = false;
                });
    }
}