import {Routes} from '@angular/router';
import {HomeComponent}   from './home.component';
import {EventDashboardRoutes} from "./event-dashboard/event-dashboard.routes";

export const HomeRoutes: Routes = [{
  path: '',
  redirectTo: 'event_dashboard',
  pathMatch: 'full'
}, {
  path: 'home',
  component: HomeComponent
}, {
  path: 'event_dashboard',
  children: EventDashboardRoutes
}];
