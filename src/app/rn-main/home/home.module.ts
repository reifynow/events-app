import {NgModule} from '@angular/core';
import {AuthService} from '../../rn-secure/auth.service';
//import {LoginService} from '../account/login/login.service';
import {HomeComponent}   from './home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RnCoreModule} from '../../rn-core/rn-core.module';
import {RouterModule} from '@angular/router';
import {AccountModule} from'../account/account.module';
import {EventDashboardModule} from "./event-dashboard/event-dashboard.module";
import {CommonModule} from "@angular/common";
@NgModule({
    imports: [
        CommonModule,
        FormsModule, NgbModule,
        ReactiveFormsModule,
        AccountModule,
        RnCoreModule,
        RouterModule,
        EventDashboardModule
    ],
    declarations: [
        HomeComponent
    ]
})
export class HomeModule {
    constructor() {
    }
}
