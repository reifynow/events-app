import {Routes} from '@angular/router';
import {EventDashboardComponent} from "./components/event-dashboard.component";
import {EventsListComponent} from "./components/events-list.component";
import {EventComponent} from "./components/event.component";
import {RegisterComponent} from "./components/register.component";
import {AuthComponent} from "./components/auth.component";
import {ManageComponent} from "./components/manage-events.component";
import {LandingComponent} from "./components/landing.component";


export const EventDashboardRoutes: Routes = [{
  path: '',
  component: EventDashboardComponent,
  children: [{
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  }, {
    path: 'landing',
    component: LandingComponent
  },{
    path: 'list',
    component: EventsListComponent
  }, {
    path: 'event',
    component: EventComponent
  }, {
    path: 'register',
    component: RegisterComponent
  }, {
    path: 'login',
    component: AuthComponent
  }, {
    path: 'manage',
    component: ManageComponent
  }]
}];
