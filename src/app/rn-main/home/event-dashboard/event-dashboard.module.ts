import { NgModule } from '@angular/core';
import {AuthService} from '../../../rn-secure/auth.service';
//import {LoginService} from '../account/login/login.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RnCoreModule} from '../../../rn-core/rn-core.module';
import { RouterModule } from '@angular/router';
import {AccountModule} from'../../account/account.module';
import {EventDashboardComponent} from "./components/event-dashboard.component";
import {DbTopNavComponent} from "./components/top-nav.component";
import {EventsListComponent} from "./components/events-list.component";
import {CalendarModule, EditorModule, TabViewModule} from "primeng/primeng";
import {CommonModule} from "@angular/common";
import {EventDashboardService} from "./event-dashboard.service";
import {EventComponent} from "./components/event.component";
import {RegisterComponent} from "./components/register.component";
import {SalesforceLoginService} from "./salesforce-login.service.comopnent";
import {LoginComponent} from "./components/login.component";
import {AuthComponent} from "./components/auth.component";
import {ManageComponent} from "./components/manage-events.component";
import {LandingComponent} from "./components/landing.component";
@NgModule({
    imports: [
        CommonModule,
        FormsModule, NgbModule,
        ReactiveFormsModule,
        AccountModule,
        RnCoreModule,
        RouterModule,
        TabViewModule,
        CalendarModule,
        EditorModule
    ],
    declarations: [
        EventDashboardComponent,
        DbTopNavComponent,
        EventsListComponent,
        EventComponent,
        RegisterComponent,
        LoginComponent,
        AuthComponent,
        ManageComponent,
        LandingComponent
    ],
    providers: [
        EventDashboardService,
        SalesforceLoginService
    ]
})
export class EventDashboardModule {
    constructor() {}
}
