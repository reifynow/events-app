import {Injectable} from '@angular/core';
import {HttpClient} from '../../../rn-secure/http-client';
import {SecureDataService} from '../../../rn-secure/secure-data.service';
import {Observable} from "rxjs/Observable";

@Injectable()
export class EventDashboardService {
    apiUrl: string = '';
    constructor(private secureDataService: SecureDataService, private http : HttpClient) {
        this.apiUrl = secureDataService.getApiUrl();
    }

    authenticate(code : any) : Observable<any> {
        return this.http.get(this.secureDataService.getAuthUrl()+'/callback?code='+code).map(res => res.json());
    }

    getAttendees(eventId) : Observable<any> {
        return this.http.get(this.apiUrl+'/attendees/'+eventId).map(res => res.json());
    }

    getSessionAttendees(sessionId) : Observable<any> {
        return this.http.get(this.apiUrl+'/session_attendees/'+sessionId).map(res => res.json());
    }

    checkEmail(eventId, email) : Observable<any> {
        return this.http.get(this.apiUrl+'/event/'+eventId+'/registered/'+email).map(res => res.json());
    }

    getEvents() : Observable<any> {
        return this.http.get(this.apiUrl+'/events').map(res => res.json());
    }

    getEvent(eventId) : Observable<any> {
        return this.http.get(this.apiUrl+'/event/'+eventId).map(res => res.json());
    }

    getSessions(eventId) : Observable<any> {
        return this.http.get(this.apiUrl+'/sessions/'+eventId).map(res => res.json());
    }

    getSession(sessionId) : Observable<any> {
        return this.http.get(this.apiUrl+'/session/'+sessionId).map(res => res.json());
    }

    addEvent(event : any) : Observable<any> {
        return this.http.post(this.apiUrl+'/event', JSON.stringify(event)).map(res => res.json());
    }

    addSession(eventId : number, session : any) : Observable<any> {
        return this.http.post(this.apiUrl+'/event/'+eventId+'/session', JSON.stringify(session)).map(res => res.json());
    }

    addAttendee(eventId: number, attendee: any) : Observable<any> {
        return this.http.post(this.apiUrl+'/event/'+eventId+'/attendee', JSON.stringify(attendee)).map(res => res.json());
    }

    addAttendeeToSession(attendeeId, sessionId) : Observable<any> {
        return this.http.post(this.apiUrl+'/session/'+sessionId+'/'+attendeeId, JSON.stringify({})).map(res => res.json());
    }
}
