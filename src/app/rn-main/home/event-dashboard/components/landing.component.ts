import {Component} from '@angular/core';

@Component({
    template: `
        <div class="jumbotron">
            <h1>Events</h1>
            <p><a class="btn btn-primary btn-lg" routerLink="../list" role="button">Events</a></p>
        </div>
    `
})
export class LandingComponent {
    constructor() {}
}