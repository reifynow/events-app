import {Component, Input, OnInit} from '@angular/core';
import {SecureDataService} from "../../../../rn-secure/secure-data.service";
import {Router} from "@angular/router";
import {AuthService} from "../../../../rn-secure/auth.service";
declare var window : any;
@Component({
  selector: 'top-nav',
  template: `
<nav class="navbar navbar-default" style="">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">SF Events</a>
    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li routerLinkActive="active"><a routerLink="list">Events</a></li>
          <li *ngIf="loggedIn" routerLinkActive="active"><a routerLink="manage">Manage</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" *ngIf="loggedIn" (click)="showMenu()" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{userProfile.firstName}} {{userProfile.lastName}} <span class="caret"></span></a>
            <a href="" (click)="navLogin($event)" *ngIf="!loggedIn">Login</a>
          <ul class="dropdown-menu" clickOutside [hidden]="hideLogout">
            <li><a (click)="logout($event)">Logout</a></li>
          </ul>
        </li>
      </ul>
        
    </div>
  </div>
</nav>
  `,
  styles: [`
    .nav-top-content {width: 257px;}
  `]
})
export class DbTopNavComponent implements OnInit {
  @Input('user') userProfile = {firstName: 'SF User', lastName: ''};
  loggedIn : boolean = false;
  hideLogout : boolean = true;
  constructor(private secureDataService : SecureDataService, private router : Router, private authService : AuthService){}
  userprofile() {}

  ngOnInit() {
    if (this.authService.isLoggedIn()) this.loggedIn = true;
    else this.loggedIn = false;
    this.authService.getUser.subscribe(user => {
      if (user) this.loggedIn = true;
    });
  }

  showMenu() {
    this.hideLogout = false;
  }

  navLogin(e ?: any) {
    if (e) e.preventDefault();
    window.location.href=this.secureDataService.getAuthUrl();
  }

  logout(e ?: any) {
    if (e) e.preventDefault();
    this.hideLogout = true;
    this.loggedIn = false;
    this.authService.setLoggedOut()
    this.router.navigate(['/event_dashboard']);
  }

  stopLink(e : any) : void {
    e.stopPropagation();
    e.preventDefault();
    return;
  }
}
