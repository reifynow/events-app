import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {EventDashboardService} from "../event-dashboard.service";

@Component({
    selector: 'event-component',
    template: `
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body" style="">
                        <h2>{{event.title__c}}</h2>
                        <small>Begins: {{event.start__c | date:'longDate'}}</small><br />
                        <small>Ends: {{event.start__c | date:'longDate'}}</small><br />
                        <small>Status: {{event.status}}</small>
                        <p style="margin-top: 25px; margin-bottom: 25px; font-size: 18px;" [innerHtml]="event.description__c"></p>
                        <div class="col-md-8 col-md-offset-2">
                            <h5>Available Sessions</h5>
                            <ul class="list-group">
                                <li *ngFor="let session of sessions" href="#" 
                                   class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h3 style="margin-top: 0; margin-bottom: 0;" class="mb-1">{{session.title__c}}</h3>
                                        <small>{{session.start__c | date:'shortDate'}} - {{session.end__c | date:'shortDate'}}</small>
                                    </div>
                                    <small>{{session.status__c}}</small>
                                    <p class="mb-1">Registration Limit: {{session.registration_limit__c}}<br/>Seats Remaining:
                                        {{session.seats_remaining__c}}</p>
                                    <button style="float:right; margin-top: -40px;" (click)="registerForSession(session)" class="btn btn-primary">Register For Session</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    `,
    styles: [`
    `]
})
export class EventComponent implements OnInit {

    event: any = {};
    sessions : Array<any> = [];
    constructor(private router: Router, private route: ActivatedRoute, private edService: EventDashboardService) {

    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if (params['eventId']) {
                this.edService.getEvent(params['eventId']).subscribe(event => {
                    if (event.length) this.event = event[0];
                });
                this.edService.getSessions(params['eventId']).subscribe(sessions => {
                   this.sessions = sessions;
                });
            }
        })
    }

    registerForSession(session) {
        this.router.navigate(['../register', {eventId: this.event.event_id__c, sessionId : session.session_id__c}], {relativeTo: this.route});
    }

}