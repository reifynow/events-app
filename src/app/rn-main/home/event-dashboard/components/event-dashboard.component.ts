import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventDashboardService} from "../event-dashboard.service";

@Component({
    selector: 'event-dashboard',
    template: `<!--  -->
    <div class="container-fluid">
        <top-nav [user]="user"></top-nav>
        <rn-breadcrumb></rn-breadcrumb>
        <!---->
        <router-outlet></router-outlet>
    </div>
    `
})
export class EventDashboardComponent implements OnInit, OnDestroy {
    user: any = {firstName: 'SF User', lastName : ''};
    events : Array<any> =[];
    constructor(private edService : EventDashboardService) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {

    }

}