import {Component, OnInit} from '@angular/core';
import {EventDashboardService} from "../event-dashboard.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../../rn-secure/auth.service";

@Component({
    selector: 'auth-component',
    template: ``
})
export class AuthComponent implements OnInit {

    constructor(private edService: EventDashboardService, private router : Router, private route : ActivatedRoute, private authService : AuthService) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (params['code']) {
                this.edService.authenticate(params['code']).subscribe(res => {
                    this.authService.setLoggedIn(res, {firstName: 'SFUser', lastName: ''});
                    this.router.navigate(['/event_dashboard']);
                });
            }
        });
    }

}