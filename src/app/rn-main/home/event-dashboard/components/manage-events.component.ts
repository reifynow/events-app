import {Component, OnInit} from '@angular/core';
import {EventDashboardService} from "../event-dashboard.service";
import {RnAlertData} from "../../../../rn-core/components/messaging/alert/rn-alert.component";

@Component({
    selector: 'manage-events',
    template: `
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Details
                    </div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            <h4>Events</h4>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Events</label>
                                    <div class="col-md-9">
                                        <select [(ngModel)]="searchEvent" (change)="updateSearchSessions()" class="form-control" name="searchevent">
                                            <option *ngFor="let event of events" [value]="event.event_id__c">{{event.title__c}}</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <h4>Sessions</h4>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Sessions</label>
                                    <div class="col-md-9">
                                        <select [(ngModel)]="searchSession" (change)="updateSearchAttendees()" name="searchsessions" class="form-control">
                                            <option *ngFor="let session of searchSessions" [value]="session.session_id__c">{{session.title__c}}</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <h4>Attendees</h4>
                            <p><span *ngFor="let attendee of searchAttendees">{{attendee.first_name__c}} {{attendee.last_name__c}}<br /></span></p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Manage Events
                    </div>
                    <div class="panel-body">
                        <rn-alert [alerts]="alerts"></rn-alert>
                        <div class="col-md-4" style="border-right: 1px solid #ccc; height: calc(100vh);">
                            <h3>Create Event</h3>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="event.title" name="title"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Start</label>
                                    <div class="col-md-9">
                                        <p-calendar [(ngModel)]="event.start" name="start"></p-calendar>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">End</label>
                                    <div class="col-md-9">
                                        <p-calendar name="end" [(ngModel)]="event.end"></p-calendar>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-9">
                                        <select class="form-control" [(ngModel)]="event.status" name="status">
                                            <option value="Draft">Draft</option>
                                            <option value="Open">Open</option>
                                            <option value="Closed">Closed</option>
                                            <option value="Sold Out">Sold Out</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Remaining Seats</label>
                                    <div class="col-md-9">
                                        <input type="number" [(ngModel)]="event.remainingSeats" name="remainingseats"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Registration Limit</label>
                                    <div class="col-md-9">
                                        <input type="number" [(ngModel)]="event.registrationLimit" name="reglimit"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-9">
                                    <p-editor [(ngModel)]="event.description" [style]="{'min-height':'320px'}" name="desc"></p-editor>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: right">
                                    <button class="btn btn-primary" (click)="createEvent()">Create Event</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4" style="border-right: 1px solid #ccc; height: calc(100vh);">
                            <h3>Create Session</h3>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-3">For Event</label>
                                    <div class="col-md-9">
                                        <select class="form-control" [(ngModel)]="sessionEvent" name="event">
                                            <option *ngFor="let event of events" [value]="event.event_id__c">{{event.title__c}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="session.title" name="title"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Start</label>
                                    <div class="col-md-9">
                                        <p-calendar [(ngModel)]="session.start" name="start"></p-calendar>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">End</label>
                                    <div class="col-md-9">
                                        <p-calendar [(ngModel)]="session.end" name="end"></p-calendar>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-9">
                                        <select class="form-control" [(ngModel)]="session.status" name="status">
                                            <option value="Draft">Draft</option>
                                            <option value="Open">Open</option>
                                            <option value="Closed">Closed</option>
                                            <option value="Sold Out">Sold Out</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Remaining Seats</label>
                                    <div class="col-md-9">
                                        <input type="number" [(ngModel)]="session.remainingSeats" name="remseats"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Registration Limit</label>
                                    <div class="col-md-9">
                                        <input type="number" [(ngModel)]="session.registrationLimit" name="reglimit"/>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: right">
                                    <button class="btn btn-primary" (click)="createSession()">Create Event</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4" style="border-right: 1px solid #ccc; height: calc(100vh);">
                            <h3>Create Attendee</h3>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-3">For Event</label>
                                    <div class="col-md-9">
                                        <select class="form-control" [(ngModel)]="attendeeEvent" (change)="updateSessionList()" name="event">
                                            <option *ngFor="let event of events" [value]="event.event_id__c">{{event.title__c}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">For Session</label>
                                    <div class="col-md-9">
                                        <select class="form-control" [(ngModel)]="attendeeSession" name="session">
                                            <option *ngFor="let session of sessions" [value]="session.session_id__c">{{session.title__c}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="attendee.firstName" name="firstname"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="attendee.lastName" name="lastname"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="attendee.email" name="email"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Phone</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" [(ngModel)]="attendee.phone" name="phone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="contupdateSearchAttrol-label col-md-3">Company</label>
                                    <div class="col-md-9">
                                        <input type="number" [(ngModel)]="attendee.company" name="compony"/>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: right">
                                    <button class="btn btn-primary" (click)="createAttendee()">Create Attendee</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class ManageComponent implements OnInit {
    public event: any = {title: '', end: null, start: null, status: 'Open', remainingSeats: 0, registrationLimit: 0, description: ''};
    public events: Array<any> = [];
    public sessions : Array<any> = [];
    public alerts: Array<RnAlertData> = [];
    session : any = {title: '', end: null, start: null, status: 'Open', remainingSeats: 0, registrationLimit: 0};
    attendee : any = {firstName: '', lastName: '', phone: '', email: '', company: ''};
    sessionEvent : any;
    attendeeEvent : any;
    attendeeSession : any;
    searchEvent: any;
    searchSession: any;
    searchSessions: any;
    searchAttendees : any;
    constructor(private edService: EventDashboardService) {

    }

    ngOnInit() {
        this.loadEvents();
    }

    loadEvents() {
        this.edService.getEvents().subscribe(events => {
            this.events = events;
        });
    }

    updateSearchSessions() {
        this.edService.getSessions(this.searchEvent).subscribe(res => {
            this.searchSessions = res;
        });
    }

    updateSearchAttendees() {
        this.edService.getSessionAttendees(this.searchSession).subscribe(res => {
            this.searchAttendees = res;
        });
    }

    updateSessionList() {
        this.edService.getSessions(this.attendeeEvent).subscribe(res => {
            this.sessions=res;
        });
    }

    createEvent() {
        this.edService.addEvent(this.event).subscribe(res => {
            if (res) this.alerts.push(new RnAlertData('success', 'New event created.'));
            this.loadEvents();
        })
    }

    createSession() {
        this.edService.addSession(this.sessionEvent, this.session).subscribe(res => {
            if (res) this.alerts.push(new RnAlertData('success', 'Session Created'));
        });
    }

    createAttendee() {
        this.edService.checkEmail(this.attendeeEvent, this.attendee.email).subscribe(idCheck => {
            if (idCheck.attendeeId !== null) {
                this.edService.addAttendeeToSession(idCheck.attendeeId, this.attendeeSession).subscribe(res => {
                    this.alerts.push(new RnAlertData('success', 'Attendee added to event/session.'));
                });
            }
            else {
                this.edService.addAttendee(this.attendeeEvent, this.attendee).subscribe(res => {
                    this.edService.addAttendeeToSession(res.result.id, this.attendeeSession).subscribe(res => {
                        this.alerts.push(new RnAlertData('success', 'Attendee added to event/session.'));
                    });
                });
            }
        });
    }
}