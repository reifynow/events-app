import {AfterViewChecked, AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {EventDashboardService} from "../event-dashboard.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'events-list',
    template: `
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="list-group">
                    <a *ngFor="let event of events" href="#" (click)="selectEvent(event, $event)"
                       class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h3 class="mb-1">{{event.title__c}}</h3>
                            <small>{{event.start__c | date:'shortDate'}} - {{event.end__c | date:'shortDate'}}</small>
                        </div>
                        <p class="mb-1">Registration Limit: {{event.registration_limit__c}}<br/>Seats Remaining:
                            {{event.seats_remaining__c}}</p>
                        <small>{{event.status__c}}</small>
                    </a>
                </div>
            </div>
        </div>
        <!--        <div class="row">
                    <div class="col-md-3">
                        <div class="panel-primary">
                            <div style="top: 50px; width: 100%; background-color: #fff; z-index: 1000; height: 25px; position:fixed;"></div>
                            <div class="panel-heading" style="position:fixed; width: 23%; top:60px; z-index: 1000;">Events</div>
                            <div class="panel-body" style="margin-top: 85px;">
                                <div class="list-group">
                                    <a *ngFor="let event of events" href="#" (click)="selectEvent(event, $event)" class="list-group-item list-group-item-action flex-column align-items-start" [class.active]="event.selected">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1">{{event.title__c}}</h5>
                                            <small>{{event.start__c | date:'shortDate'}} - {{event.end__c | date:'shortDate'}}</small>
                                        </div>
                                        <p class="mb-1">Registration Limit: {{event.registration_limit__c}}<br />Seats Remaining: {{event.seats_remaining__c}}</p>
                                        <small>{{event.status__c}}</small>
                                    </a>
                                </div>
                            </div>
                        </div>                
                    </div>
                    <div class="col-md-9">
                        <div class="panel-primary" style="position:fixed; top: 60px; width: 73%; z-index: 1000;">
                            <div class="panel-heading">Sessions</div>
                            <div *ngIf="selectedEvent" class="panel-body">
                                <h3>{{selectedEvent.title__c}}</h3>
                                <p [innerHtml]="selectedEvent.description__c"></p>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="list-group">
                                        <a *ngFor="let session of sessions" href="#" (click)="selectSession(session, $event)" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1">{{session.title__c}}</h5>
                                            <small>{{session.start__c | date:'shortDate'}} - {{session.end__c | date:'shortDate'}}</small>
                                            </div>
                                            <small>{{session.status__c}}</small>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
    `
})
export class EventsListComponent implements OnInit {
    events: Array<any> = [];

    constructor(private edService: EventDashboardService, private router: Router, private route: ActivatedRoute) {
    }

    selectEvent(ev, e ?: any) {
        if (e) e.preventDefault();
        this.router.navigate(['../event', {eventId: ev.event_id__c}], {relativeTo: this.route});

        /*        this.sessions = [];
         if (e) e.preventDefault();
         this.selectedEvent = ev;
         this.edService.getSessions(this.selectedEvent.event_id__c).subscribe(sessions => {
         this.sessions = sessions;
         });
         this.events.forEach(event => {
         if (event !== ev) event.selected = false;
         else event.selected = true;
         });*/
    }

    ngOnInit() {
        this.edService.getEvents().subscribe(events => {
            this.events = events;
            /*            if (events.length) {
             this.selectEvent(this.events[0]);
             }*/
        });
    }


}