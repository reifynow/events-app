import {Component, OnInit} from '@angular/core';
import {EventDashboardService} from "../event-dashboard.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {RnAlertData} from "../../../../rn-core/components/messaging/alert/rn-alert.component";

@Component({
    selector: 'register',
    template: `
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <rn-alert [alerts]="alerts"></rn-alert>
                        </div>
                        
                        <form class="form-horizontal" #regForm="ngForm" novalidate (submit)="register()" *ngIf="!registered">
                            <div class="form-group">
                                <label class="col-md-3 control-label">First Name</label>
                                <div class="col-md-8">
                                    <input type="text" name="firstname" [(ngModel)]="attendee.firstName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Last Name</label>
                                <div class="col-md-8">
                                    <input type="text" [(ngModel)]="attendee.lastName" name="lastname"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-8">
                                    <input type="text" [(ngModel)]="attendee.email" name="email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone</label>
                                <div class="col-md-8">
                                    <input type="text" [(ngModel)]="attendee.phone" name="phone"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Company</label>
                                <div class="col-md-8">
                                    <input type="text" [(ngModel)]="attendee.company" name="company"/>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: right;">
                                <button (click)="cancel()" class="btn btn-info">Cancel</button><button class="btn btn-primary" (click)="regForm.submit()">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class RegisterComponent implements OnInit {
    eventId: any;
    sessionId: any;
    attendee: any = {};
    registered: boolean = false;
    alerts : Array<RnAlertData> = [];
    constructor(private edService: EventDashboardService, private router : Router, private route : ActivatedRoute) {

    }

    ngOnInit() {
        this.route.params.forEach((params : Params) => {
            if (!params['sessionId'] || !params['eventId']) {
                this.router.navigate(['../../../'], {relativeTo: this.route});
            }
            else {
                this.eventId = params['eventId'];
                this.sessionId = params['sessionId'];
            }
        });
    }

    register() {
        this.edService.checkEmail(this.eventId, this.attendee.email).subscribe(idCheck => {
            if (idCheck.attendeeId !== null) { //ATTENDEE ALREADY REGISTERED TO EVENT SOMEWHERE, LET THEM REGISTER TO A SESSION //TODO: ADD CHECKING AGAINST SESSION (DUPLICATE REGISTRATIONS).
                this.edService.addAttendeeToSession(idCheck.attendeeId, this.sessionId).subscribe(res => {
                    this.registered = true;
                    this.alerts.push(new RnAlertData('success', 'You\'ve successfully registered. You\'ll get an email shortly.'));
                });
            }
            else {
                this.edService.addAttendee(this.eventId, this.attendee).subscribe(res => {
                    this.edService.addAttendeeToSession(res.result.id, this.sessionId).subscribe(res => {
                        this.registered = true;
                        this.alerts.push(new RnAlertData('success', 'You\'ve successfully registered. You\'ll get an email shortly.'));
                    });
                });
            }
        });
    }

    cancel() {
        this.router.navigate(['../../'], {relativeTo: this.route});
    }
}