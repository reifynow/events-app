export class UserProfile {
  id : string = '';
  firstName : string = '';
  lastName : string = '';
  username : string = '';
  password : string = '';
  email : string = '';
}
