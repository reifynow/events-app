import {Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {HomeComponent} from "./manage/home.component";
import {UsersComponent} from "./manage/users.component";

export const DashboardRoutes: Routes = [{
  path: '',
  component: DashboardComponent,
  children: [{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },{
    path: 'home',
    component: HomeComponent
  },{
    path: 'users',
    component: UsersComponent
  }]
}];
