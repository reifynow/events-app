import {Component, Input} from '@angular/core';

@Component({
  selector: 'page-header',
  template: `
			<div class="page-header-div">
				<div class="page-title">{{title}}</div>
			</div>
  `
})
export class PageHeaderComponent {
  @Input() title;
}
