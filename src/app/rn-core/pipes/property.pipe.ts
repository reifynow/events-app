import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'property',
    pure: false
})
export class PropertyPipe implements PipeTransform {
    transform(list: Array<any>, propertyNames: Array<string>, value: string): any {
        if (value === '') {
            return list;
        }

        return list.filter(item => {
            for (let index = 0; index < propertyNames.length; index++) {
                let propValue: string = item[propertyNames[index]];
                return propValue.toLowerCase().indexOf(value) >= 0;
            }
        });
    }
}