import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BcRouteItem, BreadcrumbService} from './breadcrumb.service';
import {Subscription} from 'rxjs/Rx';
@Component({
  selector : 'rn-breadcrumb',
  template: `
  <div class="profile-content-history">
    <i class="zmdi zmdi-city-alt"></i>
    <ul class="navigation-list-history">
      <li *ngFor="let route of routes"><a href [routerLink]="route.route">{{route.name}}</a></li>
    </ul>
  </div>

  `
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  routes : Array<BcRouteItem> = [];

  private subscriptions : Array<Subscription> = [];
  constructor(private bcService : BreadcrumbService) {

  }

  ngOnInit() {
    this.bcService.getRoutes.subscribe((routes : Array<BcRouteItem>) => {
      this.routes = routes;
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription : Subscription) => {
      subscription.unsubscribe();
    });
  }
}
