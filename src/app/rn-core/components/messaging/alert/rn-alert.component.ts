import {Component, Input, OnChanges, OnInit, SimpleChange, DoCheck} from '@angular/core';

export class RnAlertData {
  type : string; //danger, success, warning, log
  dismissible : boolean = true;
  message : string;
  timeToLive : number = null;
  constructor(type: string, message : string, timeToLive : number = 5) {
    this.type=type;
    this.message=message;
    this.timeToLive = timeToLive;
  }
}


@Component({
  selector: 'rn-alert',
  inputs: ['alerts', 'displayRoot'],
  template: `
    <ngb-alert *ngFor="let alert of alerts" [type]="alert.type" (close)="closeAlert(alert)" [dismissible]="alert.dismissible"><span [innerHtml]="alert.message"></span><ng-content></ng-content></ngb-alert>
   `,
  styles: [`
    .alert-wrapper {  
      top: 65px;
      width: 100%;
      margin:  0 auto;
      position: absolute;
    }
    .display_root {
      left: 15%;
     }
  `]
})
export class RnAlert implements DoCheck {
  @Input() alerts: Array<RnAlertData> = [];
  @Input() displayRoot : boolean = false;
  private oldAlerts : Array<RnAlertData> = [];

  closeAlert(alert) {
    let idx = this.alerts.indexOf(alert);
    if (~idx) this.alerts.splice(idx, 1);
  }

  ngDoCheck() {
    let changeDetected = false;
    let newAlert = null;
    this.alerts.forEach(alert => {
      if (!~this.oldAlerts.indexOf(alert)) {
        changeDetected = true;
        newAlert = alert;
      }
    });
    if (this.alerts.length != this.oldAlerts.length) changeDetected = true;
    if (newAlert) this.checkTimeToLive(newAlert);
    this.oldAlerts = [];
    this.alerts.forEach(alert => {
      this.oldAlerts.push(alert);
    });
  }

  checkTimeToLive(alert : RnAlertData) {
      if (alert.timeToLive) {
          setTimeout(((alert) => {
            return () => {
              this.closeAlert(alert);
            }
          })(alert), alert.timeToLive * 1000)
      }
  }



  constructor() {
  }
}
