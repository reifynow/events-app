require('rootpath');
var express = require('express');

var config = require('config.json');
var _ = require('lodash');
var nforce = require('nforce');
var moment = require('moment');
var service = {};

service.authenticate = authenticate;

service.getEvents = getEvents;
service.getEvent = getEvent;
service.createEvent = createEvent;

service.getSessions = getSessions;
service.getSession = getSession;
service.createSession = createSession;
service.checkEmail = checkEmail;
service.getAttendees = getAttendees;
service.getAttendee = getAttendee
service.createAttendee = createAttendee;

service.getAttendeesBySession = getAttendeesBySession;
service.addAttendeeIdToSession = addAttendeeIdToSession;
service.addAttendeeToSession = addAttendeeToSession;

service.getAuthUri = getAuthUri;
service.authenticate = authenticate;

service.connected = connected;

var org = nforce.createConnection({
    clientId: config.clientId,
    clientSecret: config.clientSecret,
    redirectUri: 'https://events.reifynow.com/event_dashboard/login',
    mode: 'multi',
    autoRefresh: true
});
var coreAuth;

function authenticate(authObj) {
    return new Promise(function(result, reject) {
        org.authenticate(authObj, function (err, resp) {
            if (!err) {
                result(resp);
            } else {
                reject(err);
            }
        });
    });
}

function connected() {
    return new Promise(function (result, rejected) {
        org.authenticate({
            username: config.username,
            password: config.password,
            securityToken: config.securityToken
        }, function (err, resp) {
            if (!err) {
                coreAuth = resp;
                result(org);
            }
            else {
                rejected(err);
            }
        });
    });
}

function getEvents() {
    return new Promise(function (resolve, reject) {
        var q = 'SELECT Id, event_id__c, title__c, status__c, start__c, end__c, remaining_seats__c, registration_limit__c, description__c FROM event__c';

        org.query({query: q, oauth: coreAuth}, function (err, res) {

            if (!err && res.records) {
                resolve(res.records);
            }
            reject(err);
        });
    });
}

function getSessions(eventId) {
    return new Promise(function (resolve, reject) {
        getEvent(eventId).then(function (events) {
            var q = 'SELECT Id, session_id__c, title__c, status__c, start__c, end__c, remaining_seats__c, registration_limit__c, event_id__c FROM session__c WHERE event_id__c = \'' + events[0]._fields['id'] + '\'';

            org.query({query: q, oauth: coreAuth}, function (err, resp) {

                if (!err && resp.records) {
                    resolve(resp.records);

                }
                else if (err) reject(err);
            });

        });
    });
}

function getSession(sessionId) {
    return new Promise(function (resolve, reject) {
        var q = 'SELECT Id, session_id__c, title__c, status__c, start__c, end__c, remaining_seats__c, registration_limit__c, event_id__c FROM session__c WHERE session_id__c = \'' + sessionId + '\'';
        org.query({query: q, oauth: coreAuth}, function (err, resp) {
            if (!err && resp.records) {
                resolve(resp.records);
            }
            else if (err) reject(err);
        })
    })
}

function getAttendees(eventId) {
    return new Promise(function (resolve, reject) {
        getEvent(eventId).then(function (events) {
            var q = 'SELECT attendee_id__c, first_name__c, last_name__c, email__c, phone__c, company__c FROM attendee__c WHERE event_id__c = \'' + events[0]._fields['id'] + '\'';
            org.query({query: q, oauth: coreAuth}, function (err, res) {
                if (!err && res.records) {
                    resolve(res.records);
                }
                reject(err);
            });
        });
    });
}

function getAttendee(attendeeId) {
    var q = 'SELECT Id, attendee_id__c, first_name__c, last_name__c, email__c, phone__c, company__c FROM attendee__c WHERE attendee_id__c = \'' + attendeeId + '\'';
    return new Promise(function (resolve, reject) {
        org.query({query: q, oauth: coreAuth}, function (err, res) {
            if (!err && res.records) {
                resolve(res.records);
            }
            reject(err);
        });
    })
}

function getEvent(eventId) {
    var q = 'SELECT Id, event_id__c, title__c, start__c, end__c, registration_limit__c, remaining_seats__c, description__c FROM event__c WHERE event_id__c = \'' + eventId + '\'';
    return new Promise(function (resolve, reject) {
        org.query({query: q, oauth: coreAuth}, function (err, res) {
            if (!err && res.records) {
                resolve(res.records);
            }
            reject(err);
        });
    });
}

function getAttendeesBySession(sessionId) {
    return new Promise(function (resolve, reject) {
        getSession(sessionId).then(function (sessions) {
            var q = 'SELECT Id, attendee_id__c, first_name__c, last_name__c, phone__c, email__c, event_id__c, company__c from attendee__c a WHERE a.Id IN (SELECT s.attendee_id__c from session_attendees_association__c s WHERE s.session_id__c = \'' + sessions[0]._fields['id'] + '\')';
            org.query({query: q, oauth: coreAuth}, function (err, res) {
                if (!err && res.records) {
                    resolve(res.records);
                }
                reject(err);
            });
        });
    });
}

function checkEmail(email, eventId) {
    return new Promise(function (resolve, reject) {
        getEvent(eventId).then(function (events) {

            var q = 'SELECT Id FROM attendee__c WHERE event_id__c = \'' + events[0]._fields['id'] + '\' AND email__c = \'' + email + '\'';
            org.query({query: q, oauth: coreAuth}, function (err, res) {
                if (!err && res.records) {
                    resolve(res.records);
                }
                reject(err);
            });
        });
    });
}

function createEvent(eventObj, oauth) {
    var ev = nforce.createSObject('event__c');
    ev.set('title__c', eventObj.title);
    ev.set('status__c', eventObj.status);
    ev.set('start__c', eventObj.start);
    ev.set('end__c', eventObj.end);
    ev.set('registration_limit__c', eventObj.registrationLimit);
    ev.set('remaining_seats__c', eventObj.remainingSeats);
    ev.set('description__c', eventObj.description);

    return new Promise(function (result, reject) {
        org.insert({sobject: ev, oauth: oauth}, function (err, resp) {
            if (!err) console.log('EVENT OBJECT CREATED');
            if (!err) result(true);
            else reject(err);
        });
    });
}

function createAttendee(eventId, attendeeObject) {
    return new Promise(function (result, reject) {
        getEvent(eventId).then(function (res, rej) {
            var attendee = nforce.createSObject('attendee__c');
            attendee.set('event_id__c', res[0]._fields['id']);
            attendee.set('first_name__c', attendeeObject.firstName);
            attendee.set('last_name__c', attendeeObject.lastName);
            attendee.set('email__c', attendeeObject.email);
            attendee.set('phone__c', attendeeObject.phone);
            attendee.set('company__c', attendeeObject.company);

            org.insert({sobject: attendee, oauth: coreAuth}, function (err, resp) {
                if (!err) result(resp);
                else reject(err);
            });
        });
    });
}

function createSession(eventId, sessionObject) {
    return new Promise(function (result, reject) {
        getEvent(eventId).then(function (res, rej) {
            var session = nforce.createSObject('session__c');
            session.set('event_id__c', res[0]._fields['id']);
            session.set('title__c', sessionObject.title);
            session.set('status__c', sessionObject.status);
            session.set('start__c', sessionObject.start);
            session.set('end__c', sessionObject.end);
            session.set('remaining_seats__c', sessionObject.remainingSeats);
            session.set('registration_limit__c', sessionObject.registrationLimit);
            org.insert({sobject: session, oauth: coreAuth}, function (err, resp) {
                if (!err) result(true);
                else reject(err);
            });
        });
    });
}

function addAttendeeIdToSession(attendeeId, sessionId) {

    return new Promise(function (result, reject) {
        getSession(sessionId).then(function (session) {
            var asa = nforce.createSObject('session_attendees_association__c');
            asa.set('attendee_id__c', attendeeId);
            asa.set('session_id__c', session[0]._fields['id']);
            org.insert({sobject: asa, oauth: coreAuth}, function (err, resp) {
                if (!err) result(true);
                else reject(err);
            });
        });
    });
}

function addAttendeeToSession(attendeeId, sessionId) {
    return new Promise(function (result, reject) {
        var promiseArray = [];

        if (attendeeId.length > 3) promiseArray.push(Promise.resolve([{_fields: {id: attendeeId}}]));
        else promiseArray.push(getAttendee(attendeeId));
        if (sessionId.length > 3) promiseArray.push(Promise.resolve([{_fields: {id: sessionId}}]));
        else promiseArray.push(getSession(sessionId));

        Promise.all(promiseArray).then(function (results) {
            var asa = nforce.createSObject('session_attendees_association__c');
            asa.set('attendee_id__c', results[0][0]._fields['id']);
            asa.set('session_id__c', results[1][0]._fields['id']);
            org.insert({sobject: asa, oauth: coreAuth}, function (err, resp) {
                if (!err) result(true);
                else reject(err);
            })
        }).catch(function (errors) {
            reject(errors);
        });
    })
}

function getAuthUri() {
    return org.getAuthUri();
}


module.exports = service;